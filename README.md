[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=bugs)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=coverage)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=caju-challenge)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=caju-challenge&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=caju-challenge)

## Requisitos

Alguns poucos requisitos são necessários para a execução do projeto, a saber:

- [Ambientes IOS e Android configurados](https://reactnative.dev/docs/environment-setup)

- [Node Js](https://nodejs.org/en/) Versão >= 16

## Setup IOS

Após configurar as variáveis de ambiente siga os seguintes passos para execução da aplicação:

```bash
# Instalando dependências
yarn install
```

```bash
# Instalando dependências
npx pod install
```

```bash
# Iniciando metro
yarn start
```

```bash
# Inciar IOS em modo dev
yarn ios:dev
```

## Setup Android

Após configurar as variáveis de ambiente siga os seguintes passos para execução da aplicação:

```bash
# Instalando dependências
yarn install
```

```bash
# Iniciando metro
yarn start
```

```bash
# Inciar Android em modo dev
yarn android:dev
```

## Executando testes, linter e coverage

```bash
# Executando testes
yarn test:local
```

```bash
# Executando linter
yarn lint:local
```

```bash
# Executando coverage
yarn coverage
```