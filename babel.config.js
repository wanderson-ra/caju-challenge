module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [     
        [
          "module-resolver", {
            "root": ["./src"],
            "alias": {
              "@design-system": "./src/DesignSystem",
              "@http-client": "./src/HttpClient",
              "@routes": "./src/App/Routes",
              "@screens": "./src/App/Screens",
              "@config": "./src/App/Config",
              "@services": "./src/App/Services",
              "@domains": "./src/App/Domains"
            }
          }
        ],
      'react-native-reanimated/plugin'
  ],
};
