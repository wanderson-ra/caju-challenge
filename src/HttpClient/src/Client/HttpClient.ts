import { Fetcher } from "../Fetcher";

export class HttpClient {
    constructor(private fetcher: Fetcher) {}

    public get<Response>(url: string): Promise<Response | undefined> {
        return this.fetcher.get(url);
    }

    public post<Response>(url: string, data: unknown): Promise<Response | undefined> {
        return this.fetcher.post(url, data);
    }

    public put<Response>(url: string, data: unknown): Promise<Response | undefined> {
        return this.fetcher.put(url, data);
    }

    public delete<Response>(url: string): Promise<Response | undefined> {
        return this.fetcher.delete(url);
    }
}
