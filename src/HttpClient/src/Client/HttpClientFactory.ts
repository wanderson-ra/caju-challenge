import { FetcherConfig } from "../Fetcher";
import { FetcherAxios } from "../Fetcher/Axios/FetcherAxios";
import { HttpClient } from "./HttpClient";

export class HttpClientFactory {
    public httpClient: HttpClient;

    constructor(fetcherConfig: FetcherConfig) {
        this.httpClient = new HttpClient(new FetcherAxios(fetcherConfig));
    }
}
