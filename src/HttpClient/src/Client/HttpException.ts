export class HttpException extends Error {
    constructor(public readonly httpCode?: number) {
        super();
        Object.setPrototypeOf(this, new.target.prototype);
    }
}
