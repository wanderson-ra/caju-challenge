export interface FetcherConfig {
    baseUrl: string;
    timeout: number;
    retryCount: number;
    retryDelay: number;
}
