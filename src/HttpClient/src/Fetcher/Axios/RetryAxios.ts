import { AxiosInstance } from "axios";
import * as retry from "retry-axios";

import { FetcherConfig } from "../FetcherConfig";

export type RetryAxiosProps = Required<
    Pick<FetcherConfig, "retryCount" | "retryDelay">
> & {
    axiosInstance: AxiosInstance;
};

export class RetryAxios {
    constructor(private retryAxiosProps: RetryAxiosProps) {}

    public configure(): void {
        this.addRequestRetry();
    }

    private async addRequestRetry(): Promise<void> {
        this.retryAxiosProps.axiosInstance.defaults.raxConfig = {
            instance: this.retryAxiosProps.axiosInstance,
            retry: this.retryAxiosProps.retryCount,
            retryDelay: this.retryAxiosProps.retryDelay,
            httpMethodsToRetry: ["GET", "HEAD", "OPTIONS", "DELETE", "PUT", "POST"],
            statusCodesToRetry: [[500, 599]],
        };

        retry.attach(this.retryAxiosProps.axiosInstance);
    }
}
