import axios, { AxiosInstance, AxiosResponse, AxiosError } from "axios";

import { HttpException } from "../../Client/HttpException";
import { Fetcher } from "../Fetcher";
import { FetcherConfig } from "../FetcherConfig";
import { RetryAxios } from "./RetryAxios";

export class FetcherAxios extends Fetcher {
    private axiosInstance: AxiosInstance | undefined;

    constructor(fetcherConfig: FetcherConfig) {
        super();
        this.axiosInstance = this.createAxiosInstance(fetcherConfig);
        this.configureRetry(fetcherConfig, this.axiosInstance);
    }

    private createAxiosInstance(fetcherConfig: FetcherConfig): AxiosInstance {
        return axios.create({
            baseURL: fetcherConfig.baseUrl,
            timeout: fetcherConfig.timeout,
            headers: {
                "Content-Type": "application/json;charset=utf-8",
                Accept: "application/json",
            },
        });
    }

    private configureRetry(fetcherConfig: FetcherConfig, axiosInstance: AxiosInstance) {
        const { retryCount, retryDelay } = fetcherConfig;

        const retryAxios = new RetryAxios({
            retryCount,
            axiosInstance,
            retryDelay,
        });

        retryAxios.configure();
    }

    public async get<Response>(url: string): Promise<Response | undefined> {
        try {
            return (
                this.axiosInstance &&
                this.findData(await this.axiosInstance.get<Response>(url))
            );
        } catch (error) {
            throw this.findException(error as AxiosError);
        }
    }

    public async post<Response>(
        url: string,
        data: unknown,
    ): Promise<Response | undefined> {
        try {
            return (
                this.axiosInstance &&
                this.findData(await this.axiosInstance.post(url, data))
            );
        } catch (error) {
            throw this.findException(error as AxiosError);
        }
    }

    public async put<Response>(
        url: string,
        data: unknown,
    ): Promise<Response | undefined> {
        try {
            return (
                this.axiosInstance &&
                this.findData(await this.axiosInstance.put(url, data))
            );
        } catch (error) {
            throw this.findException(error as AxiosError);
        }
    }

    public async delete<Response>(url: string): Promise<Response | undefined> {
        try {
            return (
                this.axiosInstance && this.findData(await this.axiosInstance.delete(url))
            );
        } catch (error) {
            throw this.findException(error as AxiosError);
        }
    }

    private findData<Response>(response: AxiosResponse<Response>): Response | undefined {
        if (response.data) {
            return response.data;
        }
    }

    private findException(error: AxiosError): HttpException {
        return new HttpException(error?.response?.status);
    }
}
