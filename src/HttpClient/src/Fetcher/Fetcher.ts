export abstract class Fetcher {
    abstract get<Response>(url: string): Promise<Response | undefined>;
    abstract post<Response>(url: string, data: unknown): Promise<Response | undefined>;
    abstract put<Response>(url: string, data: unknown): Promise<Response | undefined>;
    abstract delete<Response>(url: string): Promise<Response | undefined>;
}
