import React from "react";
import { ViewProps } from "react-native";

import {
    AlignItemsProps,
    BackgroundProps,
    BorderProps,
    ColorProps,
    FlexDirectionProps,
    FlexProps,
    JustifyContentProps,
    LayoutProps,
    SpaceProps,
} from "styled-system";

export type ContainerProps = SpaceProps &
    BackgroundProps &
    ColorProps &
    BorderProps &
    LayoutProps &
    FlexDirectionProps &
    AlignItemsProps &
    FlexProps &
    JustifyContentProps &
    Pick<
        ViewProps,
        | "accessibilityHint"
        | "accessibilityLabel"
        | "accessible"
        | "accessibilityState"
        | "accessibilityValue"
        | "testID"
    > & {
        children?: React.ReactElement | React.ReactElement[] | null;
    };
