import styled from "styled-components/native";
import {
    alignItems,
    background,
    border,
    color,
    compose,
    flex,
    justifyContent,
    layout,
    space,
} from "styled-system";

import { ContainerProps } from "./ContainerProps";

export const ContainerView = styled.View<ContainerProps>`
    ${compose(
        space,
        background,
        color,
        border,
        layout,
        flex,
        justifyContent,
        alignItems,
    )};
`;
