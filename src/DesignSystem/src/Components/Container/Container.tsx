import React from "react";

import { ContainerProps } from "./ContainerProps";
import { ContainerView } from "./styles";

export const Container = (props: ContainerProps) => {
    return <ContainerView testID="Container" {...props} />;
};
