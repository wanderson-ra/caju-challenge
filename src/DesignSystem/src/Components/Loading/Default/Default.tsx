import React from "react";

import { DefaultLoadingProps } from "./DefaultProps";
import { Container, Loading, ModalContainer } from "./styles";

export const DefaultLoading = ({ visible }: DefaultLoadingProps) => {
    return (
        <ModalContainer testID="DefaultLoading" isVisible={visible}>
            <Container>
                <Loading />
            </Container>
        </ModalContainer>
    );
};
