export type DefaultLoadingProps = {
    visible: boolean;
    testID?: string;
};
