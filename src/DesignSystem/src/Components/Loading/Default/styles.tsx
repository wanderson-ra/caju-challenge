import Modal from "react-native-modal";
import { heightPercentageToDP as height } from "react-native-responsive-screen";

import LottieView from "lottie-react-native";
import styled from "styled-components/native";

import DefaultLoading from "../../../Assets/animations/DefaultLoading.json";

export const ModalContainer = styled(Modal).attrs(props => ({
    backdropColor: props.theme.colors["grayScale-03"],
    animationIn: "fadeIn",
    animationOut: "fadeOut",
    animationInTiming: 500,
    backdropOpacity: 0.8,
    backdropTransitionInTiming: 500,
    backdropTransitionOutTiming: 500,
    useNativeDriver: true,
}))``;

export const Container = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const Loading = styled(LottieView).attrs({
    source: DefaultLoading,
    loop: true,
    autoPlay: true,
    speed: 1.5,
})`
    height: ${height(18)}px;
    width: ${height(18)}px;
`;
