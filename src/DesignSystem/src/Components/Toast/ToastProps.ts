import { ToastProps } from "react-native-toast-message";

export type ToastType = "success" | "error" | "default" | "info";
export type ToastPosition = "top" | "bottom";

export type ShowToastProps = {
    message: string;
    title?: string;
    position?: ToastPosition;
    autoHide?: boolean;
    type?: ToastType;
    time?: number;
    onShowToast?: () => void;
    onCloseToast?: () => void;
    onPressToast?: () => void;
};

export type BaseProps = ToastProps & { testID?: string };
