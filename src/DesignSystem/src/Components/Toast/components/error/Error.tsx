import React from "react";
import { ToastProps } from "react-native-toast-message";

import { BaseToast } from "../base";

export const Error = (props: ToastProps) => {
    return <BaseToast testID="Error" {...props} />;
};
