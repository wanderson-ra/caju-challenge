import React from "react";
import { ToastProps } from "react-native-toast-message";

import { BaseToast } from "../base";

export const Success = (props: ToastProps) => {
    return <BaseToast testID="Success" {...props} />;
};
