import React from "react";
import { ToastProps } from "react-native-toast-message";

import { BaseToast } from "../base";

export const Default = (props: ToastProps) => {
    return <BaseToast testID="Default" {...props} />;
};
