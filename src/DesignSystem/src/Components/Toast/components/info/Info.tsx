import React from "react";
import { ToastProps } from "react-native-toast-message";

import { BaseToast } from "../base";

export const Info = (props: ToastProps) => {
    return <BaseToast testID="Info" {...props} />;
};
