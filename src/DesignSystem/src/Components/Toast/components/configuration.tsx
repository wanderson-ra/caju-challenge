import React from "react";
import { ToastProps } from "react-native-toast-message";

import { Default } from "./default";
import { Error } from "./error";
import { Info } from "./info";
import { Success } from "./success";

export const configuration = {
    success: (props: ToastProps) => <Success {...props} />,
    error: (props: ToastProps) => <Error {...props} />,
    info: (props: ToastProps) => <Info {...props} />,
    default: (props: ToastProps) => <Default {...props} />,
};
