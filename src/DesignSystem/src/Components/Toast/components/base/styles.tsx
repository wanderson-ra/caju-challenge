import { BaseToast } from "react-native-toast-message";

import styled from "styled-components/native";

import { ColorNameTheme } from "../../../../Theme/ITheme";
import { ToastType } from "../../ToastProps";

type BaseToastContainerProps = {
    toastType?: ToastType | string;
    testID?: string;
};

const MAPPER_TYPE_WITH_COLOR: Record<string, ColorNameTheme> = {
    success: "green-01",
    error: "red-01",
    info: "blue-01",
    default: "grayScale-00",
};

export const BaseToastContainer = styled(BaseToast).attrs<BaseToastContainerProps>(
    ({ theme: { colors, radii, fontSizes, fonts, space }, toastType = "default" }) => ({
        text2NumberOfLines: 2,
        contentContainerStyle: {
            backgroundColor: MAPPER_TYPE_WITH_COLOR[toastType],
            borderRadius: radii["corner-00"],
            paddingHorizontal: space["spacing-00"],
        },
        text1Style: {
            fontSize: fontSizes.medium,
            fontFamily: fonts.medium,
            color: colors["grayScale-02"],
        },
        text2Style: {
            fontSize: fontSizes.small,
            fontFamily: fonts.medium,
            color: colors["grayScale-02"],
        },
    }),
)<BaseToastContainerProps>`
    border-left-color: ${({ theme: { colors }, toastType = "default" }) =>
        colors[MAPPER_TYPE_WITH_COLOR[toastType]]};
    background-color: ${({ theme: { colors }, toastType = "default" }) =>
        colors[MAPPER_TYPE_WITH_COLOR[toastType]]};
    border-radius: ${({ theme: { radii } }) => radii["corner-00"]}px;
`;
