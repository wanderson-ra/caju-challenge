import React from "react";

import { BaseProps } from "../../ToastProps";
import { BaseToastContainer } from "./styles";

export const BaseToast = (props: BaseProps) => {
    return <BaseToastContainer testID="BaseToast" toastType={props.type} {...props} />;
};
