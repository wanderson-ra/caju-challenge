import Toast from "react-native-toast-message";

import { ShowToastProps } from "../ToastProps";

export type UseToastResponse = {
    showToast: (showToastProps: ShowToastProps) => void;
    closeToast: () => void;
};

export function useToast(): UseToastResponse {
    const showToast = ({
        message,
        autoHide = true,
        onCloseToast,
        onPressToast,
        onShowToast,
        position = "bottom",
        title,
        type = "default",
        time = 4000,
    }: ShowToastProps) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            position: position,
            autoHide: autoHide,
            visibilityTime: time,
            onHide: onCloseToast,
            onPress: onPressToast,
            onShow: onShowToast,
        });
    };

    const closeToast = () => {
        Toast.hide();
    };

    return { closeToast, showToast };
}
