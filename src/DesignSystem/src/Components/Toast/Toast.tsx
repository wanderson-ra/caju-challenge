import React from "react";
import { heightPercentageToDP as height } from "react-native-responsive-screen";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import ToastMessage from "react-native-toast-message";

import { configuration } from "./components/configuration";

const OFF_SET = height(3);

export const Toast = () => {
    const { bottom, top } = useSafeAreaInsets();

    return (
        <ToastMessage
            config={configuration}
            topOffset={top + OFF_SET}
            bottomOffset={bottom + OFF_SET}
            keyboardOffset={bottom + OFF_SET}
        />
    );
};
