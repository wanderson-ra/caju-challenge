import React from "react";
import { Text } from "react-native";

import { TypographyBase } from "./styles";
import { TypographyProps } from "./TypographyProps";

export const Typography = ({ ...props }: TypographyProps) => (
    <TypographyBase testID="Text" as={Text} {...props} fontSize={props.fontSize}>
        {props.children}
    </TypographyBase>
);
