import React from "react";
import { TextProps } from "react-native";

import { TextAlignProps, FontSizeProps, LineHeightProps } from "styled-system";

import {
    FontSizeNameTheme,
    FontFamilyNameTheme,
    ColorNameTheme,
} from "../../Theme/ITheme";

export type TypographyProps = TextProps & {
    children: React.ReactNode;
    fontSize: FontSizeNameTheme;
    fontFamily: FontFamilyNameTheme;
    color?: ColorNameTheme;
};

export type TypographyBaseProps = TextAlignProps &
    TypographyProps &
    FontSizeProps &
    LineHeightProps;
