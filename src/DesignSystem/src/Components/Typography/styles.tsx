import styled from "styled-components/native";
import { textAlign, lineHeight, fontSize } from "styled-system";

import { TypographyBaseProps } from "./TypographyProps";

export const TypographyBase = styled.Text<TypographyBaseProps>`
    font-family: ${props => props.theme.fonts[props.fontFamily]};
    color: ${({ theme, color }) =>
        color ? theme.colors[color] : theme.colors["grayScale-00"]};
    ${fontSize};
    ${textAlign};
    ${lineHeight};
`;
