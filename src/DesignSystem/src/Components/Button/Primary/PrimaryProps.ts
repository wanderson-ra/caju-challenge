import { ButtonProps } from "../ButtonProps";

export type PrimaryProps = ButtonProps;
