import React from "react";
import { TouchableOpacity } from "react-native";

import { PrimaryProps } from "./PrimaryProps";
import { PrimaryContainer, Title } from "./styles";

export const Primary = ({ title, ...props }: PrimaryProps) => {
    return (
        <PrimaryContainer as={TouchableOpacity} testID="Touch" {...props}>
            <Title testID="Title">{title}</Title>
        </PrimaryContainer>
    );
};
