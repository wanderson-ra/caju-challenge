import { TouchableOpacity } from "react-native";
import { isIphoneX } from "react-native-iphone-x-helper";
import { heightPercentageToDP as height } from "react-native-responsive-screen";

import styled from "styled-components/native";
import { background, border, color, compose, space } from "styled-system";

import { Typography } from "../../Typography";
import { containerOpacityVariant } from "../variants";

export const OutlineContainer = styled(TouchableOpacity).attrs({
    borderRadius: "corner-00",
    borderWidth: "stroke-02",
    borderColor: "grayScale-01",
    px: "spacing-04",
    activeOpacity: 0.4,
})`
    ${containerOpacityVariant}
    ${compose(space, background, color, border)};
    align-items: center;
    justify-content: center;
    flex-direction: row;
    height: ${isIphoneX() ? height(7) : height(8)}px;
`;

export const Title = styled(Typography).attrs({
    fontFamily: "semi-bold",
    fontSize: "large",
    color: "grayScale-00",
    numberOfLines: 1,
})``;
