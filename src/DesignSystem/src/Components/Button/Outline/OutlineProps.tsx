import { ButtonProps } from "../ButtonProps";

export type OutlineProps = ButtonProps;
