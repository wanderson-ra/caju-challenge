import React from "react";

import { OutlineProps } from "./OutlineProps";
import { OutlineContainer, Title } from "./styles";

export const Outline = ({ title, ...props }: OutlineProps) => {
    return (
        <OutlineContainer testID="Touch" {...props}>
            <Title testID="Title">{title}</Title>
        </OutlineContainer>
    );
};
