import { Outline } from "./Outline/Outline";
import { Primary } from "./Primary/Primary";

export const Button = {
    Primary,
    Outline,
};
