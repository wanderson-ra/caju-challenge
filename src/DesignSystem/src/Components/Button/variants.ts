import { variant } from "styled-system";

export const containerOpacityVariant = variant({
    prop: "disabled",
    variants: {
        true: {
            opacity: 0.5,
        },
        false: {
            opacity: 1,
        },
    },
});
