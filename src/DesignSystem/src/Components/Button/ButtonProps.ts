import { TouchableOpacityProps } from "react-native";

export type ButtonProps = Omit<TouchableOpacityProps, "activeOpacity"> & {
    title: string;
};
