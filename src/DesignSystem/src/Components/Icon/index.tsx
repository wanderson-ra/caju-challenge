import React from "react";
import VectorIcon from "react-native-vector-icons/MaterialCommunityIcons";

import styled from "styled-components/native";
import { color, compose, fontSize } from "styled-system";

export type TVectorIcon = React.ComponentProps<typeof VectorIcon>;

const IconBase = ({ ...rest }: TVectorIcon) => <VectorIcon {...rest} />;

export const Icon = styled(IconBase)`
    ${compose(color, fontSize)}
`;
