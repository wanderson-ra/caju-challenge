import { ColorNameTheme } from "../../Theme/ITheme";
export type HeaderProps = {
    backAction: () => void;
    backgroundColor?: ColorNameTheme;
    testID?: string;
};
