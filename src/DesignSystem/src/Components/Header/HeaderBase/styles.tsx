import { heightPercentageToDP as height } from "react-native-responsive-screen";

import styled from "styled-components/native";

import { Container } from "../../Container";

export const MIN_HEIGHT = height(8);

export const HeaderBase = styled(Container).attrs({
    px: "spacing-02",
})`
    min-height: ${MIN_HEIGHT}px;
`;
