import React from "react";

import { HeaderProps } from "../HeaderProps";

export type OnlyBackIconProps = Pick<
    HeaderProps,
    "backAction" | "backgroundColor" | "testID"
> & {
    children?: React.ReactElement;
};
