import { Platform } from "react-native";
import { heightPercentageToDP as height } from "react-native-responsive-screen";

import styled from "styled-components/native";

import { Container } from "../../Container";
import { Icon } from "../../Icon";
import { MIN_HEIGHT } from "../HeaderBase/styles";

export const ContainerOnlyBackIcon = styled(Container)`
    justify-content: flex-start;
    align-items: center;
    min-height: ${MIN_HEIGHT}px;
    flex-direction: row;
`;

export const WrapperBackIcon = styled.TouchableOpacity`
    height: ${({ theme: { fontSizes } }) => fontSizes.iconLarge + height(1)}px;
    width: ${({ theme: { fontSizes } }) => fontSizes.iconLarge + height(1)}px;
    border-radius: ${({ theme: { fontSizes } }) =>
        (fontSizes.iconLarge + height(1)) / 2}px;
    justify-content: center;
    align-items: center;
`;

export const BackIcon = styled(Icon).attrs({
    name: Platform.OS === "android" ? "arrow-left" : "chevron-left",
    fontSize: "iconLarge",
    color: "grayScale-00",
})``;
