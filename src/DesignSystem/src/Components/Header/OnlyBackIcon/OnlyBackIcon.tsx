import React from "react";
import { View } from "react-native";

import { HeaderBase } from "../HeaderBase/styles";
import { OnlyBackIconProps } from "./OnlyBackIconProps";
import { BackIcon, ContainerOnlyBackIcon, WrapperBackIcon } from "./styles";

export const OnlyBackIcon = ({
    backAction,
    backgroundColor,
    children = <View />,
}: OnlyBackIconProps) => {
    return (
        <HeaderBase testID="OnlyBackIcon" backgroundColor={backgroundColor}>
            <ContainerOnlyBackIcon>
                <WrapperBackIcon testID="BackAction" onPress={backAction}>
                    <BackIcon />
                </WrapperBackIcon>
            </ContainerOnlyBackIcon>
            {children}
        </HeaderBase>
    );
};
