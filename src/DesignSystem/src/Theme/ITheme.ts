import { Theme } from "styled-system";

export type ColorNameTheme =
    | "grayScale-00"
    | "grayScale-01"
    | "grayScale-02"
    | "grayScale-03"
    | "green-01"
    | "blue-01"
    | "red-01";

export type FontSizeNameTheme = "micro" | "small" | "medium" | "large" | "extraLarge";

export type IconSizeNameTheme =
    | "iconSmall"
    | "iconMedium"
    | "iconLarge"
    | "iconExtraLarge"
    | "iconBigger";

export type SpaceSizeNameTheme =
    | "spacing-00"
    | "spacing-02"
    | "spacing-04"
    | "spacing-03";

export type BorderRadiusNameSizeTheme =
    | "corner-00"
    | "corner-02"
    | "corner-03"
    | "corner-04";

export type BorderSizeNameTheme = "stroke-00" | "stroke-01" | "stroke-02" | "stroke-03";

export type FontFamilyNameTheme = "bold" | "regular" | "medium" | "semi-bold";

export type ColorTheme = Record<ColorNameTheme, string>;
export type FontSizeTheme = Record<FontSizeNameTheme, number> &
    Record<IconSizeNameTheme, number>;
export type FontFamilyTheme = Record<FontFamilyNameTheme, string>;

export type SpaceSizeTheme = Record<SpaceSizeNameTheme, number>;
export type BorderRadiusSizeTheme = Record<BorderRadiusNameSizeTheme, number>;
export type BorderSizeTheme = Record<BorderSizeNameTheme, number>;

export interface ITheme extends Theme {
    colors: ColorTheme;
    fontSizes: FontSizeTheme;
    fonts: FontFamilyTheme;
    space: SpaceSizeTheme;
    radii: BorderRadiusSizeTheme;
    borderWidths: BorderSizeTheme;
}
