export { darkTheme } from "./colors/dark";
export { lightTheme } from "./colors/light";
export type { ITheme } from "./ITheme";
