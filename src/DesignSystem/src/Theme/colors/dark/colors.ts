import { ColorTheme } from "../../ITheme";

export const colors: ColorTheme = {
    "grayScale-00": "#FFFFFF",
    "grayScale-01": "#F9F9F9",
    "grayScale-02": "#000000",
    "grayScale-03": "#FCFCFC",
    "green-01": "#B2F9AE",
    "blue-01": "#99F0FF",
    "red-01": "#FFC2B0",
};
