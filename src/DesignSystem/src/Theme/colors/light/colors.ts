import { ColorTheme } from "../../ITheme";

export const colors: ColorTheme = {
    "grayScale-00": "#04021D",
    "grayScale-01": "#CCCCCC",
    "grayScale-02": "#FFFFFF",
    "grayScale-03": "#B2B2B2",
    "green-01": "#188D49",
    "blue-01": "#0083DB",
    "red-01": "#DB2A40",
};
