import { ITheme } from "../../ITheme";
import { fontSizes, fonts } from "../../sizes/fonts";
import { radii, space, borderWidths } from "../../sizes/scales";
import { colors } from "./colors";

export const lightTheme: ITheme = {
    colors,
    fontSizes,
    fonts,
    radii,
    space,
    borderWidths,
};
