import { RFValue } from "react-native-responsive-fontsize";
import { heightPercentageToDP as height } from "react-native-responsive-screen";

import { FontFamilyTheme, FontSizeTheme } from "../ITheme";

export const fonts: FontFamilyTheme = {
    regular: "Poppins-Regular",
    medium: "Poppins-Medium",
    bold: "Poppins-Bold",
    "semi-bold": "Poppins-SemiBold",
};

export const fontSizes: FontSizeTheme = {
    micro: RFValue(12, height(100)),
    small: RFValue(14, height(100)),
    medium: RFValue(16, height(100)),
    large: RFValue(18, height(100)),
    extraLarge: RFValue(27, height(100)),
    iconSmall: height(2),
    iconMedium: height(3),
    iconLarge: height(4),
    iconExtraLarge: height(5),
    iconBigger: height(6),
};
