import { heightPercentageToDP as height } from "react-native-responsive-screen";

import { SpaceSizeTheme, BorderRadiusSizeTheme, BorderSizeTheme } from "../ITheme";

export const space: SpaceSizeTheme = {
    "spacing-00": height(1),
    "spacing-02": height(2),
    "spacing-03": height(3),
    "spacing-04": height(4),
};

export const borderWidths: BorderSizeTheme = {
    "stroke-00": 0.5,
    "stroke-01": 1,
    "stroke-02": 2,
    "stroke-03": 3,
};

export const radii: BorderRadiusSizeTheme = {
    "corner-00": height(1),
    "corner-02": height(2),
    "corner-03": height(3),
    "corner-04": height(4),
};
