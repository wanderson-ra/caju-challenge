import React from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";

import { ThemeProvider } from "styled-components/native";

import { Toast } from "../Components/Toast/Toast";
import { darkTheme, lightTheme } from "../Theme";

type ProviderProps = {
    children: React.ReactNode;
    mode: "dark" | "light";
};

export const Provider = ({ children, mode }: ProviderProps) => {
    return (
        <SafeAreaProvider>
            <ThemeProvider theme={mode === "dark" ? darkTheme : lightTheme}>
                {children}
                <Toast />
            </ThemeProvider>
        </SafeAreaProvider>
    );
};
