import { HttpClientFactory } from "@http-client/src";

import { ENVIRONMENT } from "@config/Environment";

export const httpClient = new HttpClientFactory({
    baseUrl: ENVIRONMENT.apiBaseUrl,
    retryCount: 3,
    retryDelay: 200,
    timeout: 30000,
}).httpClient;
