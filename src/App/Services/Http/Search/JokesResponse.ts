import { Joke } from "@domains/Joke";

export class JokesResponse {
    public readonly results!: Joke[];
    public readonly current_page!: number;
    public readonly next_page!: number;
    public readonly previous_page!: number;
}
