import { httpClient } from "../HttpClient";
import { JokesResponse } from "./JokesResponse";

export async function getSearches({ pageParam = 1 }): Promise<JokesResponse | undefined> {
    return httpClient.get<JokesResponse>(`search?page=${pageParam}`);
}
