import Environment from "react-native-config";

export const ENVIRONMENT = {
    apiBaseUrl: Environment.API_BASE_URL,
};
