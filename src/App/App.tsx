import React from "react";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { QueryClient, QueryClientProvider } from "react-query";

import { Provider as OpenFinanceProvider } from "@design-system/src";

import { ApplicationNavigator } from "@routes/routes";

const queryClient = new QueryClient();

export const App = () => {
    return (
        <OpenFinanceProvider mode="light">
            <QueryClientProvider client={queryClient}>
                <GestureHandlerRootView style={{ flex: 1 }}>
                    <ApplicationNavigator />
                </GestureHandlerRootView>
            </QueryClientProvider>
        </OpenFinanceProvider>
    );
};
