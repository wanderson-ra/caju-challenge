import { CommonActions, createNavigationContainerRef } from "@react-navigation/native";

import { RootStackParamsList, Routes } from "../types/types";

export const navigationRef = createNavigationContainerRef<RootStackParamsList>();

export function navigate(name: keyof RootStackParamsList, params?: any) {
    if (navigationRef.isReady()) {
        navigationRef.navigate(name, params);
    }
}

export function navigateAndReset(routes: Routes[] = [], index = 0) {
    if (navigationRef.isReady()) {
        navigationRef.dispatch(
            CommonActions.reset({
                index,
                routes: routes.map(route => {
                    return { name: route.name };
                }),
            }),
        );
    }
}

export function navigateAndSimpleReset({ name, params }: Routes, index = 0) {
    if (navigationRef.isReady()) {
        navigationRef.dispatch(
            CommonActions.reset({
                index,
                routes: [{ name, params }],
            }),
        );
    }
}
