import React from "react";

import { About } from "@screens/About";
import { Home } from "@screens/Home";
import { Jokes } from "@screens/Jokes";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { RootStackParamsList } from "./types/types";
import { navigationRef } from "./utils";

const Stack = createNativeStackNavigator<RootStackParamsList>();

export const ApplicationNavigator = () => {
    return (
        <NavigationContainer ref={navigationRef}>
            <Stack.Navigator
                initialRouteName="Home"
                screenOptions={{
                    headerShown: false,
                    animation: "fade",
                    animationTypeForReplace: "push",
                }}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Jokes" component={Jokes} />
                <Stack.Screen name="About" component={About} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};
