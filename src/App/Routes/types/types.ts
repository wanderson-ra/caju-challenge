import { Route } from "@react-navigation/native";
import type { StackScreenProps } from "@react-navigation/stack";

import { AboutParamList, AboutParams } from "./About";
import { HomeParamList, HomeParams } from "./Home";
import { JokesParamList, JokesParams } from "./Jokes";

export type RootStackParamsList = HomeParamList & JokesParamList & AboutParamList;

export type Screens = keyof RootStackParamsList;

export type ScreenParams = HomeParams & JokesParams & AboutParams;

export type Routes = Omit<Route<Extract<Screens, string>>, "key" | "params"> & {
    params?: ScreenParams;
};

export type RootStackScreenProps<T extends keyof RootStackParamsList> = StackScreenProps<
    RootStackParamsList,
    T
>;

declare global {
    namespace ReactNavigation {
        interface RooParamsList extends RootStackParamsList {}
    }
}
