import React from "react";

import { navigate } from "@routes/utils";

import { SafeAreaContainer, Title } from "../styles";
import { HomeContainer, JokesButton, AboutButton } from "./styles";

export const Home = () => {
    const navigateToPiadas = () => {
        navigate("Jokes");
    };

    const navigateToSobre = () => {
        navigate("About");
    };

    return (
        <SafeAreaContainer>
            <HomeContainer>
                <Title testID="HomeTitle">HaHa Piadas</Title>
                <JokesButton
                    testID="JokesButton"
                    title="Piadas"
                    onPress={navigateToPiadas}
                />
                <AboutButton
                    testID="AboutButton"
                    title="Sobre"
                    onPress={navigateToSobre}
                />
            </HomeContainer>
        </SafeAreaContainer>
    );
};
