import { heightPercentageToDP as height } from "react-native-responsive-screen";

import { Container, Button } from "@design-system/src";

import styled from "styled-components/native";

export const HomeContainer = styled(Container).attrs({
    pl: "spacing-03",
    pr: "spacing-03",
    pt: "spacing-04",
})`
    justify-content: flex-start;
    flex: 1;
`;

export const JokesButton = styled(Button.Primary)`
    margin-bottom: ${height(3)}px;
`;

export const AboutButton = styled(Button.Outline)``;
