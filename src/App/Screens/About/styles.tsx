import { Typography } from "@design-system/src";

import styled from "styled-components/native";

export const Message = styled(Typography).attrs({
    fontSize: "small",
    fontFamily: "regular",
})`
    text-align: justify;
    padding-left: ${({ theme: { space } }) => space["spacing-03"]}px;
    padding-right: ${({ theme: { space } }) => space["spacing-03"]}px;
    margin-top: ${({ theme: { space } }) => space["spacing-02"]}px;
`;
