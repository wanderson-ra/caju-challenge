import React from "react";

import { Header } from "@design-system/src";

import { useNavigation } from "@react-navigation/native";

import { SafeAreaContainer, Title } from "../styles";
import { Message } from "./styles";

export const About = () => {
    const { goBack } = useNavigation();

    return (
        <SafeAreaContainer>
            <Header.OnlyBackIcon testID="AboutBackAction" backAction={goBack} />
            <Title testID="AboutTitle">Sobre</Title>
            <Message testID="AboutMessage">
                is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                has been the standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type and scrambled it to make a type specimen
                book. It has survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets containing
                Lorem Ipsum passages, and more recently with desktop publishing software
                like Aldus PageMaker including versions of Lorem Ipsum.
            </Message>
        </SafeAreaContainer>
    );
};
