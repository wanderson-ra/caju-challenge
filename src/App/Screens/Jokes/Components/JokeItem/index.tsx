import React, { memo } from "react";

import { Joke } from "@domains/Joke";

import { Description, JokeItemContainer } from "./styles";

type JokeItemProps = {
    joke: Joke;
};

export const JokeItem = memo(({ joke }: JokeItemProps) => {
    return (
        <JokeItemContainer>
            <Description testID="Description">{joke.joke}</Description>
        </JokeItemContainer>
    );
});
