import { Container, Typography } from "@design-system/src";

import styled from "styled-components/native";

export const JokeItemContainer = styled(Container).attrs({
    p: "spacing-02",
})`
    justify-content: flex-start;
`;

export const Description = styled(Typography).attrs({
    fontSize: "small",
    fontFamily: "regular",
})`
    text-align: justify;
`;
