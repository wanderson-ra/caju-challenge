import React, { useCallback, useEffect } from "react";
import { FlatList, ListRenderItemInfo } from "react-native";

import { Loading, Header, useToast } from "@design-system/src";

import { Joke } from "@domains/Joke";

import { useNavigation } from "@react-navigation/native";

import { SafeAreaContainer, Title } from "../styles";
import { JokeItem } from "./Components/JokeItem";
import { useFetchJokes } from "./Hooks/UseFetchJokes";
import { Separator, ActivityIndicator, EmptyData } from "./styles";

export const Jokes = () => {
    const { showToast } = useToast();
    const { goBack } = useNavigation();

    const {
        isLoading,
        data,
        hasNextPage,
        fetchNextPage,
        isFetchingNextPage,
        isError,
        reset,
    } = useFetchJokes();

    const renderItem = useCallback(({ item }: ListRenderItemInfo<Joke>) => {
        return <JokeItem joke={item} />;
    }, []);

    const renderListFooterComponent = useCallback(() => {
        return isFetchingNextPage ? <ActivityIndicator /> : null;
    }, [isFetchingNextPage]);

    const renderListHeaderComponent = useCallback(() => {
        return <Title>Piadas</Title>;
    }, []);

    const renderEmptyData = useCallback(() => {
        return !isLoading ? <EmptyData>Não foram encontradas piadas.</EmptyData> : null;
    }, [isLoading]);

    const loadMore = useCallback(() => {
        if (hasNextPage && !isError) {
            fetchNextPage();
        }
    }, [fetchNextPage, hasNextPage, isError]);

    useEffect(() => {
        if (isError && !isFetchingNextPage) {
            showToast({
                title: "Ops",
                message: "Ocorreu um erro inesperado.",
                autoHide: true,
                time: 3000,
                type: "error",
                onCloseToast: reset,
            });
        }
    }, [isError, isFetchingNextPage, reset, showToast]);

    return (
        <SafeAreaContainer>
            <Header.OnlyBackIcon testID="JokesHeader" backAction={goBack} />

            <FlatList
                testID="ListJokes"
                showsVerticalScrollIndicator={false}
                renderItem={renderItem}
                data={
                    data ? data.pages.map(page => (page ? page.results : [])).flat() : []
                }
                keyExtractor={joke => joke.id}
                ItemSeparatorComponent={Separator}
                onEndReached={loadMore}
                onEndReachedThreshold={0.3}
                ListFooterComponent={renderListFooterComponent}
                ListHeaderComponent={renderListHeaderComponent}
                ListEmptyComponent={renderEmptyData}
            />

            <Loading.Default testID="JokesLoading" visible={isLoading} />
        </SafeAreaContainer>
    );
};
