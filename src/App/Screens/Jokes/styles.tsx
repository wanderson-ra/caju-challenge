import { Container, Typography } from "@design-system/src";

import styled from "styled-components/native";

export const Separator = styled(Container).attrs({
    bg: "grayScale-03",
})`
    height: 0.5px;
`;

export const ActivityIndicator = styled.ActivityIndicator.attrs(theme => ({
    size: "large",
    color: theme.theme.colors["grayScale-00"],
}))`
    padding: ${({ theme: { space } }) => space["spacing-03"]}px;
`;

export const EmptyData = styled(Typography).attrs({
    fontSize: "medium",
    fontFamily: "regular",
})`
    text-align: center;
`;
