import { useState } from "react";
import { useInfiniteQuery, UseInfiniteQueryResult } from "react-query";

import { getSearches } from "@services/Http/Search";

import { JokesResponse } from "../../../Services/Http/Search/JokesResponse";

export type UseFetchJokesProps = Pick<
    UseInfiniteQueryResult<JokesResponse | undefined>,
    | "data"
    | "isError"
    | "isLoading"
    | "isFetchingNextPage"
    | "fetchNextPage"
    | "hasNextPage"
> & { reset: () => void };

export function useFetchJokes(): UseFetchJokesProps {
    const [isError, setIsError] = useState(false);

    const reset = () => {
        setIsError(false);
    };

    const onError = () => setIsError(true);

    const getNextPageParam = (lastPage: JokesResponse | undefined) =>
        lastPage?.next_page ? lastPage.next_page : lastPage;

    const { isLoading, data, hasNextPage, fetchNextPage, isFetchingNextPage } =
        useInfiniteQuery("search", getSearches, {
            onError,
            getNextPageParam,
        });

    return {
        isLoading,
        data,
        hasNextPage,
        fetchNextPage,
        isFetchingNextPage,
        isError,
        reset,
    };
}
