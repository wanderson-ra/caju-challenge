import { Typography } from "@design-system/src";

import styled from "styled-components/native";

export const SafeAreaContainer = styled.SafeAreaView`
    flex: 1;
    background-color: ${({ theme: { colors } }) => colors["grayScale-02"]};
`;

export const Title = styled(Typography).attrs({
    fontSize: "extraLarge",
    fontFamily: "semi-bold",
})`
    text-align: center;
    margin-bottom: ${({ theme: { space } }) => space["spacing-04"]}px;
`;
