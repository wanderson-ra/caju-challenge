declare module "react-native-config" {
    interface Environment {
        API_BASE_URL: string;
    }

    const Environment: Environment;

    export default Environment;
}
