module.exports = {
    root: true,
    parser: "@typescript-eslint/parser",
    env: {
        browser: false,
        es6: true,
    },
    extends: [      
        "eslint:recommended",   
        "@react-native-community",
        "plugin:react/recommended",    
        "plugin:@typescript-eslint/recommended",      
        "plugin:prettier/recommended",
        "eslint:recommended",    
        "plugin:react-hooks/recommended",
        "plugin:jest/recommended"
    ],
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: "module",
    },
    plugins: ["react-hooks","prettier", "@typescript-eslint", "eslint-plugin-import-helpers"],
    rules: {
        "react/prop-types": "off",
        "@typescript-eslint/no-namespace":"off",
        "@typescript-eslint/ban-types": "off",
        "react/display-name":"off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-empty-interface":"off",
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn",
        "no-console": "error",
        "no-shadow": "off",
        "no-irregular-whitespace": 0,
        "@typescript-eslint/no-explicit-any": "off",        
        "prettier/prettier": "error",
        "react-native/no-inline-styles": "off",     
        "no-catch-shadow": "off",  
        "no-new":"off",  
        "no-unused-vars": "off",
        "@typescript-eslint/no-unused-vars": [
            "error",
            {
                argsIgnorePattern: "_",
            },
        ],
        "import-helpers/order-imports": [
            "warn",
            {
                newlinesBetween: "always",
                groups: [
                    "/^react/",
                    "/^react-native/",
                    "/@design-system/",
                    "/@http-client/",
                    "/@routes/",
                    "/@screens/",
                    "/@services/",
                    "/@domains/",
                    "module",
                    ["parent", "sibling", "index"],
                ],
                alphabetize: { order: "asc", ignoreCase: true },
            },
        ],
    }
};
