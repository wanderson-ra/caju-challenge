jest.useFakeTimers()
jest.mock("react-native/Libraries/Animated/NativeAnimatedHelper");

jest.mock("react-native-safe-area-context");

jest.mock("@react-navigation/native", () => {
  return {
      createNavigationContainerRef: jest.fn().mockReturnValue({
        isReady: jest.fn(),
    }),
      useIsFocused: jest.fn(),
      useRoute: jest.fn(),
      useNavigation: jest.fn(),
      useNavigation: jest.fn().mockReturnValue({
        navigate: jest.fn(),
    }),
  };
});

jest.mock("./src/DesignSystem/src/Components/Toast", () => {
  return {
      useToast: jest.fn().mockReturnValue({
          showToast: jest.fn(),
          closeToast: jest.fn(),
      }),
  };
});