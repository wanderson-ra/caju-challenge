import React from "react";

import { Loading } from "../../../../../../src/DesignSystem/src/Components/Loading";
import { createRender } from "../../../utils/test-wrapper";

describe("Tests of Loading.Default", () => {
    it("Should loading default visible", async () => {
        const loadingDefault = await createRender(<Loading.Default visible={true} />);

        const loadingView = loadingDefault.root.findByProps({ testID: "DefaultLoading" });

        expect(loadingView.props.isVisible).toBe(true);
    });

    it("Should not loading default visible", async () => {
        const loadingDefault = await createRender(<Loading.Default visible={false} />);
        const loadingView = loadingDefault.root.findByProps({ testID: "DefaultLoading" });

        expect(loadingView.props.isVisible).toBe(false);
    });
});
