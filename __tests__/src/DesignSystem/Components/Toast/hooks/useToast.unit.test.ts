import Toast from "react-native-toast-message";

import { renderHook, act } from "@testing-library/react-native";

import { useToast } from "../../../../../../src/DesignSystem/src/Components/Toast/hooks/useToast";
import { ShowToastProps } from "../../../../../../src/DesignSystem/src/Components/Toast/ToastProps";

describe("Tests of useToast", () => {
    it("Should execute show toast", () => {
        const mockedShowToast = jest.spyOn(Toast, "show");
        const showToastProps: ShowToastProps = {
            message: "anyMessage",
            autoHide: false,
            onCloseToast: jest.fn(),
            onPressToast: jest.fn(),
            onShowToast: jest.fn(),
            position: "bottom",
            time: 3000,
            title: "anyTitle",
            type: "default",
        };

        const { result } = renderHook(() => useToast());

        act(() => {
            result.current.showToast(showToastProps);
        });

        expect(mockedShowToast).toHaveBeenCalledWith({
            type: showToastProps.type,
            text1: showToastProps.title,
            text2: showToastProps.message,
            position: showToastProps.position,
            autoHide: showToastProps.autoHide,
            visibilityTime: showToastProps.time,
            onHide: showToastProps.onCloseToast,
            onPress: showToastProps.onPressToast,
            onShow: showToastProps.onShowToast,
        });
    });

    it("Should execute show toast default props", () => {
        const mockedShowToast = jest.spyOn(Toast, "show");
        const showToastProps: ShowToastProps = {
            message: "anyMessage",
            onCloseToast: jest.fn(),
            onPressToast: jest.fn(),
            onShowToast: jest.fn(),
            title: "anyTitle",
        };

        const { result } = renderHook(() => useToast());

        act(() => {
            result.current.showToast(showToastProps);
        });

        expect(mockedShowToast).toHaveBeenCalledWith({
            type: "default",
            text1: showToastProps.title,
            text2: showToastProps.message,
            position: "bottom",
            autoHide: true,
            visibilityTime: 4000,
            onHide: showToastProps.onCloseToast,
            onPress: showToastProps.onPressToast,
            onShow: showToastProps.onShowToast,
        });
    });
    it("Should execute close toast", () => {
        const mockedHideToast = jest.spyOn(Toast, "hide");

        const { result } = renderHook(() => useToast());

        act(() => {
            result.current.closeToast();
        });

        expect(mockedHideToast).toHaveBeenCalled();
    });
});
