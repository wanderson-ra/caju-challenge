import React from "react";
import * as SafeAreaContext from "react-native-safe-area-context";
import ToastMessage from "react-native-toast-message";

import { when } from "jest-when";

import { configuration } from "../../../../../src/DesignSystem/src/Components/Toast/components/configuration";
import { Toast } from "../../../../../src/DesignSystem/src/Components/Toast/Toast";
import { createRender } from "../../utils/test-wrapper";

describe("Tests of Toast", () => {
    const useSafeAreaInsetsMocked = jest.spyOn(SafeAreaContext, "useSafeAreaInsets");

    it("Should render with success", async () => {
        const bottom = 3;
        const top = 4;

        when(useSafeAreaInsetsMocked)
            .calledWith()
            .mockReturnValue({
                bottom,
                top,
            } as any);

        const toast = await createRender(<Toast />);

        const toastMessage = toast.root.findByType(ToastMessage);

        expect(toastMessage.props.topOffset).toEqual(top + 40);
        expect(toastMessage.props.bottomOffset).toEqual(bottom + 40);
        expect(toastMessage.props.keyboardOffset).toEqual(bottom + 40);
        expect(toastMessage.props.config).toBe(configuration);
    });
});
