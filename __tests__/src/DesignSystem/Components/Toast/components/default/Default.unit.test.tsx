import React from "react";

import { Default } from "../../../../../../../src/DesignSystem/src/Components/Toast/components/default/Default";
import { createRender } from "../../../../utils/test-wrapper";

describe("Tests of Default", () => {
    it("Should render with success", async () => {
        const toast = await createRender(<Default />);

        const defaultToast = toast.root.findByProps({ testID: "Default" });

        expect(defaultToast).toBeTruthy();
    });
});
