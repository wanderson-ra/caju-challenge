import React from "react";

import { BaseToast } from "../../../../../../../src/DesignSystem/src/Components/Toast/components/base/Base";
import { createRender } from "../../../../utils/test-wrapper";

describe("Tests of BaseToast", () => {
    it("Should render with success", async () => {
        const type = "default";

        const toast = await createRender(<BaseToast type={type} />);

        const baseToast = toast.root.findByProps({ testID: "BaseToast" });

        expect(baseToast.props.toastType).toBe(type);
    });
});
