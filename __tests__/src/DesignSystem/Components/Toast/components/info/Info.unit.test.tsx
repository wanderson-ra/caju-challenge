import React from "react";

import { Info } from "../../../../../../../src/DesignSystem/src/Components/Toast/components/info/Info";
import { createRender } from "../../../../utils/test-wrapper";

describe("Tests of Info", () => {
    it("Should render with success", async () => {
        const toast = await createRender(<Info />);

        const infoToast = toast.root.findByProps({ testID: "Info" });

        expect(infoToast).toBeTruthy();
    });
});
