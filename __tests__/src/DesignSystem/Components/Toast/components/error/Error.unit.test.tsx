import React from "react";

import { Error } from "../../../../../../../src/DesignSystem/src/Components/Toast/components/error/Error";
import { createRender } from "../../../../utils/test-wrapper";

describe("Tests of Error", () => {
    it("Should render with success", async () => {
        const toast = await createRender(<Error />);

        const errorToast = toast.root.findByProps({ testID: "Error" });

        expect(errorToast).toBeTruthy();
    });
});
