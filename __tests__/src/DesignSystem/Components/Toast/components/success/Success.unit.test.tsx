import React from "react";

import { Success } from "../../../../../../../src/DesignSystem/src/Components/Toast/components/success";
import { createRender } from "../../../../utils/test-wrapper";

describe("Tests of Success", () => {
    it("Should render with success", async () => {
        const toast = await createRender(<Success />);

        const successToast = toast.root.findByProps({ testID: "Success" });

        expect(successToast).toBeTruthy();
    });
});
