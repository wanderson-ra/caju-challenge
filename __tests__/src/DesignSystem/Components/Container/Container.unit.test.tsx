import React from "react";

import { Container } from "../../../../../src/DesignSystem/src/Components/Container/Container";
import { createRender } from "../../utils/test-wrapper";

describe("Tests of Container", () => {
    it("Should render with success", async () => {
        const bg = "primary-01";

        const container = createRender(<Container bg={bg} />);

        const containerView = (await container).root.findByProps({ testID: "Container" });

        expect(containerView.props.bg).toEqual(bg);
    });
});
