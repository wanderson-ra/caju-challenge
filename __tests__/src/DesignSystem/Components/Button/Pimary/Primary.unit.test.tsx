import React from "react";

import { Primary } from "../../../../../../src/DesignSystem/src/Components/Button/Primary/Primary";
import { createRender, act } from "../../../utils/test-wrapper";

describe("Test of Primary Button", () => {
    it("Should asserts props", async () => {
        const title = "anyTitle";
        const onPress = jest.fn();

        const primaryButton = await createRender(
            <Primary title={title} onPress={onPress} />,
        );

        const titleView = primaryButton.root.findByProps({ testID: "Title" });
        const touch = primaryButton.root.findByProps({ testID: "Touch" });

        expect(titleView.props.children).toEqual(title);

        act(() => {
            touch.props.onPress();
        });

        expect(onPress).toBeCalled();
    });
});
