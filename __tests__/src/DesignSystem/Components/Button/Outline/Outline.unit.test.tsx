import React from "react";

import { Outline } from "../../../../../../src/DesignSystem/src/Components/Button/Outline/Outline";
import { createRender, act } from "../../../utils/test-wrapper";

describe("Test of Outline Button", () => {
    it("Should asserts props", async () => {
        const title = "anyTitle";
        const onPress = jest.fn();

        const OutlineButton = await createRender(
            <Outline title={title} onPress={onPress} />,
        );

        const titleView = OutlineButton.root.findByProps({ testID: "Title" });
        const touch = OutlineButton.root.findByProps({ testID: "Touch" });

        expect(titleView.props.children).toEqual(title);

        act(() => {
            touch.props.onPress();
        });

        expect(onPress).toBeCalled();
    });
});
