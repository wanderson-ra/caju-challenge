import React from "react";

import { Typography } from "../../../../../src/DesignSystem/src/Components/Typography/Typography";
import {
    FontFamilyNameTheme,
    FontSizeNameTheme,
} from "../../../../../src/DesignSystem/src/Theme/ITheme";
import { createRender } from "../../utils/test-wrapper";

describe("Tests of Typography", () => {
    it("Should asserts children", async () => {
        const children = "anyChildren";
        const fontFamily: FontFamilyNameTheme = "medium";
        const fontSize: FontSizeNameTheme = "medium";

        const typography = await createRender(
            <Typography fontFamily={fontFamily} fontSize={fontSize}>
                {children}
            </Typography>,
        );

        const textView = typography.root.findByProps({ testID: "Text" });

        expect(textView.props.children).toEqual(children);
        expect(textView.props.fontFamily).toEqual(fontFamily);
        expect(textView.props.fontSize).toEqual(fontSize);
    });
});
