import React from "react";

import { OnlyBackIcon } from "../../../../../../src/DesignSystem/src/Components/Header/OnlyBackIcon/OnlyBackIcon";
import { createRender, act } from "../../../utils/test-wrapper";

describe("Tests of OnlyBackIcon", () => {
    it("Should be called back button", async () => {
        const backAction = jest.fn();
        const backgroundColor = "grayScale-03";

        const onlyBackIcon = await createRender(
            <OnlyBackIcon backAction={backAction} backgroundColor={backgroundColor} />,
        );

        const backActionView = onlyBackIcon.root.findByProps({ testID: "BackAction" });
        const headerBaseView = onlyBackIcon.root.findByProps({ testID: "OnlyBackIcon" });

        act(() => {
            backActionView.props.onPress();
        });

        expect(backAction).toHaveBeenCalled();
        expect(headerBaseView.props.backgroundColor).toEqual(backgroundColor);
    });
});
