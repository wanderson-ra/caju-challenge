import React from "react";
import renderer from "react-test-renderer";
export { act } from "react-test-renderer";

import { ThemeProvider } from "styled-components/native";

import { lightTheme } from "../../../../src/DesignSystem/src/Theme";

export const createRender = async (
    children: React.ReactElement,
): Promise<renderer.ReactTestRenderer> => {
    let renderedView: renderer.ReactTestRenderer;

    await renderer.act(async () => {
        renderedView = renderer.create(
            <ThemeProvider theme={lightTheme}>{children}</ThemeProvider>,
        );
    });

    return renderedView;
};
