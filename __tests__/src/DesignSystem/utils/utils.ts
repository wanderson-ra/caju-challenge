import * as HelperIphoneX from "react-native-iphone-x-helper";

import { when } from "jest-when";

export const mockIphoneX = (isIphoneX = true): void => {
    const mockedHelperIphoneX = jest.spyOn(HelperIphoneX, "isIphoneX");
    when(mockedHelperIphoneX).calledWith().mockReturnValue(isIphoneX);
};
