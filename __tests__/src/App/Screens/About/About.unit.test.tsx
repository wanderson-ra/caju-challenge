import React from "react";

import * as Navigation from "@react-navigation/native";
import { when } from "jest-when";

import { About } from "../../../../../src/App/Screens/About";
import { createRender, act } from "../../utils/renderer-wrapper";

describe("Tests of About", () => {
    it("Should be clicked in back button", async () => {
        const goBack = jest.fn();
        const title = "Sobre";
        const useNavigationMocked = jest.spyOn(Navigation, "useNavigation");

        when(useNavigationMocked)
            .calledWith()
            .mockReturnValue({ goBack } as any);

        const about = await createRender(<About />);

        const header = about.root.findByProps({ testID: "AboutBackAction" });
        const aboutTitle = about.root.findByProps({ testID: "AboutTitle" });

        act(() => {
            header.props.backAction();
        });

        expect(aboutTitle.props.children).toEqual(title);
        expect(goBack).toBeCalled();
    });
});
