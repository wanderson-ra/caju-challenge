import React from "react";

import * as Navigation from "@react-navigation/native";
import { when, resetAllWhenMocks } from "jest-when";

import { Jokes } from "../../../../../src/App/Screens/Jokes";
import * as UseFetchJokes from "../../../../../src/App/Screens/Jokes/Hooks/UseFetchJokes";
import * as DesigneSystem from "../../../../../src/DesignSystem/src";
import { createRender, act } from "../../utils/renderer-wrapper";

describe("Tests of Joke", () => {
    beforeEach(() => {
        jest.clearAllMocks();
        resetAllWhenMocks();
    });

    const mockedUseFetchJokes = jest.spyOn(UseFetchJokes, "useFetchJokes");

    const useFetchJokesProps = {
        isLoading: false,
        data: {
            pageParams: [],
            pages: [
                {
                    results: [{ id: "1", joke: "any" }],
                    current_page: 1,
                    next_page: 1,
                    previous_page: 1,
                },
            ],
        },
        hasNextPage: false,
        fetchNextPage: jest.fn(),
        isFetchingNextPage: false,
        isError: false,
        reset: jest.fn(),
    } as any;

    it("Should be clicked in back button", async () => {
        const goBack = jest.fn();

        const useNavigationMocked = jest.spyOn(Navigation, "useNavigation");

        when(useNavigationMocked)
            .calledWith()
            .mockReturnValue({ goBack } as any);

        const jokes = await createRender(<Jokes />);

        const jokesHeader = jokes.root.findByProps({ testID: "JokesHeader" });

        act(() => {
            jokesHeader.props.backAction();
        });

        expect(goBack).toBeCalled();
    });

    it("Should be checked lista data is equal to fetch data", async () => {
        when(mockedUseFetchJokes).calledWith().mockReturnValue(useFetchJokesProps);

        const jokes = await createRender(<Jokes />);

        const listJokes = jokes.root.findByProps({ testID: "ListJokes" });

        expect(listJokes.props.data).toEqual(useFetchJokesProps.data?.pages[0].results);
    });

    it("Should be called load more jokes", async () => {
        const newUseFetchJokesProps = {
            ...useFetchJokesProps,
            hasNextPage: true,
            isError: false,
        };

        when(mockedUseFetchJokes).calledWith().mockReturnValue(newUseFetchJokesProps);

        const jokes = await createRender(<Jokes />);

        const listJokes = jokes.root.findByProps({ testID: "ListJokes" });

        act(() => {
            listJokes.props.onEndReached();
        });

        expect(useFetchJokesProps.fetchNextPage).toBeCalled();
    });

    it("Should not be called load more jokes because it's don't have next page", async () => {
        const newUseFetchJokesProps = {
            ...useFetchJokesProps,
            hasNextPage: false,
            isError: false,
        };

        when(mockedUseFetchJokes).calledWith().mockReturnValue(newUseFetchJokesProps);

        const jokes = await createRender(<Jokes />);

        const listJokes = jokes.root.findByProps({ testID: "ListJokes" });

        act(() => {
            listJokes.props.onEndReached();
        });

        expect(useFetchJokesProps.fetchNextPage).not.toBeCalled();
    });

    it("Should be visible jokes loading", async () => {
        useFetchJokesProps.isLoading = true;

        when(mockedUseFetchJokes)
            .calledWith()
            .mockReturnValue(useFetchJokesProps as any);

        const jokes = await createRender(<Jokes />);

        const jokesLoading = jokes.root.findByProps({ testID: "JokesLoading" });

        expect(jokesLoading.props.visible).toEqual(useFetchJokesProps.isLoading);
    });

    it("Should be showed error toast", async () => {
        const newUseFetchJokesProps = {
            ...useFetchJokesProps,
            isError: true,
        };

        const useToastMocked = jest.spyOn(DesigneSystem, "useToast");
        const showToast = jest.fn();

        when(useToastMocked)
            .calledWith()
            .mockReturnValue({ closeToast: jest.fn(), showToast });

        when(mockedUseFetchJokes).calledWith().mockReturnValue(newUseFetchJokesProps);

        await createRender(<Jokes />);

        expect(showToast).toBeCalled();
    });
});
