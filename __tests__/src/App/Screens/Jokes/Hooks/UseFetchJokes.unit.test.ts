import ReactQuery from "react-query";

import { renderHook, act } from "@testing-library/react-native";
import { when } from "jest-when";

import { useFetchJokes } from "../../../../../../src/App/Screens/Jokes/Hooks/UseFetchJokes";

jest.mock("react-query", () => {
    return {
        useInfiniteQuery: jest.fn(),
    };
});

describe("Tests of useFetchJokes", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    const mockedUseInfiniteQuery = jest.spyOn(ReactQuery, "useInfiniteQuery");

    const useFetchJokesProps = {
        isLoading: false,
        data: undefined,
        hasNextPage: false,
        fetchNextPage: jest.fn(),
        isFetchingNextPage: false,
    };

    it("Should be returned data", () => {
        const newUseFetchJokesProps = {
            ...useFetchJokesProps,
            data: {
                pages: [
                    {
                        results: [{ id: 1, joke: "any" }],
                        current_page: 1,
                        next_page: 1,
                        previous_page: 1,
                    },
                ],
            } as any,
        };

        when(mockedUseInfiniteQuery)
            .calledWith("search", expect.any(Function), expect.any(Object))
            .mockReturnValue(newUseFetchJokesProps as any);

        const { result } = renderHook(() => useFetchJokes());

        expect(result.current.data).toEqual(newUseFetchJokesProps.data);
        expect(result.current.hasNextPage).toEqual(false);
        expect(result.current.isFetchingNextPage).toEqual(false);
        expect(result.current.isError).toEqual(false);
        expect(result.current.isLoading).toEqual(false);
    });

    it("Should be returned error and reset", () => {
        when(mockedUseInfiniteQuery)
            .calledWith("search", expect.any(Function), expect.any(Object))
            .mockReturnValue(useFetchJokesProps as any);

        const { result } = renderHook(() => useFetchJokes());

        const queryOptions = mockedUseInfiniteQuery.mock.calls[0][2];

        act(() => {
            queryOptions?.onError?.("");
        });

        expect(result.current.isError).toEqual(true);

        expect(result.current.data).toEqual(undefined);
        expect(result.current.hasNextPage).toEqual(false);
        expect(result.current.isFetchingNextPage).toEqual(false);
        expect(result.current.isLoading).toEqual(false);

        act(() => {
            result.current.reset();
        });

        expect(result.current.isError).toEqual(false);
    });

    it("Should be returned last page 2", () => {
        when(mockedUseInfiniteQuery)
            .calledWith("search", expect.any(Function), expect.any(Object))
            .mockReturnValue(useFetchJokesProps as any);

        const { result } = renderHook(() => useFetchJokes());

        const queryOptions = mockedUseInfiniteQuery.mock.calls[0][2];

        const page = {
            results: [{ id: "1", joke: "any" }],
            current_page: 1,
            next_page: 2,
            previous_page: 1,
        };

        const nextPage = queryOptions?.getNextPageParam?.(page, []);

        expect(nextPage).toEqual(2);

        expect(result.current.isError).toEqual(false);
        expect(result.current.data).toEqual(undefined);
        expect(result.current.hasNextPage).toEqual(false);
        expect(result.current.isFetchingNextPage).toEqual(false);
        expect(result.current.isLoading).toEqual(false);
    });

    it("Should be returned last page", () => {
        when(mockedUseInfiniteQuery)
            .calledWith("search", expect.any(Function), expect.any(Object))
            .mockReturnValue(useFetchJokesProps as any);

        const { result } = renderHook(() => useFetchJokes());

        const queryOptions = mockedUseInfiniteQuery.mock.calls[0][2];

        const page = {
            results: [{ id: "1", joke: "any" }],
            current_page: 1,
            next_page: undefined,
            previous_page: 1,
        };

        const nextPage = queryOptions?.getNextPageParam?.(page, []);

        expect(nextPage).toEqual(page);

        expect(result.current.isError).toEqual(false);
        expect(result.current.data).toEqual(undefined);
        expect(result.current.hasNextPage).toEqual(false);
        expect(result.current.isFetchingNextPage).toEqual(false);
        expect(result.current.isLoading).toEqual(false);
    });
});
