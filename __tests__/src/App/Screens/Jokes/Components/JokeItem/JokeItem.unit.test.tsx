import React from "react";

import { JokeItem } from "../../../../../../../src/App/Screens/Jokes/Components/JokeItem";
import { createRender } from "../../../../utils/renderer-wrapper";

describe("Tests of JokeItem", () => {
    it("Assert description", async () => {
        const joke = "anyJoke";
        const id = "1";

        const jokeItem = await createRender(<JokeItem joke={{ id, joke }} />);

        const description = jokeItem.root.findByProps({ testID: "Description" });

        expect(description.props.children).toEqual(joke);
    });
});
