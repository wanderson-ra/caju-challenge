import React from "react";

import * as NavigationUtils from "@routes/utils";

import { Home } from "../../../../../src/App/Screens/Home/index";
import { createRender, act } from "../../utils/renderer-wrapper";

describe("Tests of Home", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    const navigateMocked = jest.spyOn(NavigationUtils, "navigate");

    it("Should be clicked in about button", async () => {
        const home = await createRender(<Home />);

        const aboutButton = home.root.findByProps({ testID: "AboutButton" });

        act(() => {
            aboutButton.props.onPress();
        });

        expect(navigateMocked).toBeCalledWith("About");
    });

    it("Should be clicked in jokes button", async () => {
        const home = await createRender(<Home />);

        const jokesButton = home.root.findByProps({ testID: "JokesButton" });

        act(() => {
            jokesButton.props.onPress();
        });

        expect(navigateMocked).toBeCalledWith("Jokes");
    });

    it("Check home title", async () => {
        const title = "HaHa Piadas";
        const home = await createRender(<Home />);

        const homeTitle = home.root.findByProps({ testID: "HomeTitle" });

        expect(homeTitle.props.children).toEqual(title);
    });
});
