import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import renderer from "react-test-renderer";
export { act } from "react-test-renderer";

import { ThemeProvider } from "styled-components/native";

import { lightTheme } from "../../../../src/DesignSystem/src/Theme/colors/light/index";

let renderedView: renderer.ReactTestRenderer;
const queryClient = new QueryClient();

export const createRender = async (
    children: React.ReactElement,
): Promise<renderer.ReactTestRenderer> => {
    await renderer.act(async () => {
        renderedView = renderer.create(
            <QueryClientProvider client={queryClient}>
                <ThemeProvider theme={lightTheme}>{children}</ThemeProvider>
            </QueryClientProvider>,
        );
    });

    return renderedView;
};
