import * as factory from "factory.ts";
import faker from "faker";

import { MockData } from "./MockData";

export const mockDataFactory = factory.Sync.makeFactory<MockData>({
    firstName: factory.each(() => faker.name.firstName()),
    lastName: factory.each(() => faker.name.lastName()),
});
