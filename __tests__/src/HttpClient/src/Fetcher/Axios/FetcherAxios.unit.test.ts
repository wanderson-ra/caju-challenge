import nock, { cleanAll } from "nock";

import { HttpException } from "../../../../../../src/HttpClient/src/Client/HttpException";
import { FetcherAxios } from "../../../../../../src/HttpClient/src/Fetcher/Axios/FetcherAxios";
import { FetcherConfig } from "../../../../../../src/HttpClient/src/Fetcher/FetcherConfig";
import { mockDataFactory } from "../../../dataBuilders/MockDataFactory";
import { getError } from "../../../utils/utils";

describe("Tests of FetcherAxios", () => {
    beforeEach(() => {
        jest.clearAllMocks();
        cleanAll();
    });

    const fetcherConfig: FetcherConfig = {
        baseUrl: "http://localhost",
        retryCount: 0,
        timeout: 30000,
        retryDelay: 300,
    };

    const fetchAxios = new FetcherAxios(fetcherConfig);

    describe("Tests with success", () => {
        it("Should be returned success on get", async () => {
            const mockData = mockDataFactory.buildList(3);

            nock(fetcherConfig.baseUrl).get("/test").reply(200, mockData);

            const getResponse = await fetchAxios.get("test");

            expect(getResponse).toEqual(mockData);
        });

        it("Should be returned success on get without response", async () => {
            nock(fetcherConfig.baseUrl).get("/test").reply(200);

            const getResponse = await fetchAxios.get("test");

            expect(getResponse).toEqual(undefined);
        });

        it("Should be returned success on post", async () => {
            const mockData = mockDataFactory.buildList(3);
            const mockDataPost = mockDataFactory.build();

            const matchBody = JSON.stringify(mockData);

            nock(fetcherConfig.baseUrl).post("/test", matchBody).reply(201, mockDataPost);

            const getResponse = await fetchAxios.post("test", mockData);

            expect(getResponse).toEqual(mockDataPost);
        });

        it("Should be returned success on put", async () => {
            const mockData = mockDataFactory.buildList(3);
            const mockDataPost = mockDataFactory.build();

            const matchBody = JSON.stringify(mockData);

            nock(fetcherConfig.baseUrl).put("/test", matchBody).reply(200, mockDataPost);

            const getResponse = await fetchAxios.put("test", mockData);

            expect(getResponse).toEqual(mockDataPost);
        });

        it("Should be returned success on delete", async () => {
            const mockData = mockDataFactory.buildList(3);

            nock(fetcherConfig.baseUrl).delete("/test").reply(200, mockData);

            const getResponse = await fetchAxios.delete("test");

            expect(getResponse).toEqual(mockData);
        });
    });

    describe("Tests with error", () => {
        it("Should be returned error on get with response", async () => {
            const httpCodeError = 422;

            nock(fetcherConfig.baseUrl).get("/test").reply(httpCodeError);

            const error = await getError<HttpException>(async () =>
                fetchAxios.get("test"),
            );

            expect(error).toBeInstanceOf(HttpException);
            expect(error).toHaveProperty("httpCode", httpCodeError);
        });

        it("Should ber returned get error without response", async () => {
            nock(fetcherConfig.baseUrl).get("/test").reply(500);

            const error = await getError<HttpException>(async () =>
                fetchAxios.get("test"),
            );

            expect(error).toBeInstanceOf(HttpException);
            expect(error).toHaveProperty("httpCode", 500);
        });

        it("Should returned any error", async () => {
            nock(fetcherConfig.baseUrl).get("/test/any").reply(500);

            const error = await getError<HttpException>(async () =>
                fetchAxios.get("test"),
            );

            expect(error).toBeInstanceOf(HttpException);
            expect(error).toHaveProperty("httpCode", undefined);
        });

        it("Should be returned error on post", async () => {
            const mockData = mockDataFactory.buildList(3);

            const matchBody = JSON.stringify(mockData);

            const httpCodeError = 422;

            nock(fetcherConfig.baseUrl).post("/test", matchBody).reply(httpCodeError);

            const error = await getError<HttpException>(async () =>
                fetchAxios.post("test", mockData),
            );

            expect(error).toBeInstanceOf(HttpException);
            expect(error).toHaveProperty("httpCode", httpCodeError);
        });

        it("Should be returned error on put", async () => {
            const mockData = mockDataFactory.buildList(3);

            const matchBody = JSON.stringify(mockData);

            const httpCodeError = 422;

            nock(fetcherConfig.baseUrl).put("/test", matchBody).reply(httpCodeError);

            const error = await getError<HttpException>(async () =>
                fetchAxios.put("test", mockData),
            );

            expect(error).toBeInstanceOf(HttpException);
            expect(error).toHaveProperty("httpCode", httpCodeError);
        });

        it("Should be returned error on delete", async () => {
            const httpCodeError = 422;

            nock(fetcherConfig.baseUrl).delete("/test").reply(httpCodeError);

            const error = await getError<HttpException>(async () =>
                fetchAxios.delete("test"),
            );

            expect(error).toBeInstanceOf(HttpException);
            expect(error).toHaveProperty("httpCode", httpCodeError);
        });
    });
});
