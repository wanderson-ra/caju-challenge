import {
    RetryAxiosProps,
    RetryAxios,
} from "../../../../../../src/HttpClient/src/Fetcher/Axios/RetryAxios";

describe("Tests of RetryAxios", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it("Should be returned success to retry", async () => {
        const axiosInstance = {
            interceptors: { response: { use: jest.fn() } },
            defaults: {
                raxConfig: {
                    instance: undefined,
                    retry: 0,
                    retryDelay: 0,
                    onRetryAttempt: jest.fn(),
                },
            },
        };

        const retryAxiosProps: RetryAxiosProps = {
            retryCount: 3,
            axiosInstance: axiosInstance as any,
            retryDelay: 300,
        };

        const retryAxios = new RetryAxios(retryAxiosProps);
        retryAxios.configure();

        expect(axiosInstance.defaults.raxConfig.retry).toEqual(
            retryAxiosProps.retryCount,
        );
        expect(axiosInstance.defaults.raxConfig.retryDelay).toEqual(
            retryAxiosProps.retryDelay,
        );
        expect(axiosInstance.defaults.raxConfig.instance).toEqual(axiosInstance);
    });
});
