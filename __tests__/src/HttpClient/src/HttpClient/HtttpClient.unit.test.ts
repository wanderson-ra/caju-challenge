import { mock } from "jest-mock-extended";

import { HttpClient } from "../../../../../src/HttpClient/src/Client/HttpClient";
import { Fetcher } from "../../../../../src/HttpClient/src/Fetcher/Fetcher";
import { MockData } from "../../dataBuilders/MockData";
import { mockDataFactory } from "../../dataBuilders/MockDataFactory";

describe("Tests of HttpClient", () => {
    it("Should be called get with success", async () => {
        const urlToCall = "anyUrl";

        const mockData = mockDataFactory.build();

        const mockedFetcher = mock<Fetcher>();
        mockedFetcher.get.calledWith(urlToCall).mockResolvedValue(mockData);

        const mockDataResponse = await new HttpClient(mockedFetcher).get<MockData>(
            urlToCall,
        );

        expect(mockData.firstName).toEqual(mockDataResponse?.firstName);
        expect(mockData.lastName).toEqual(mockDataResponse?.lastName);
        expect(mockedFetcher.get).toBeCalledWith(urlToCall);
    });

    it("Should be called post with success", async () => {
        const urlToCall = "anyUrl";

        const mockData = mockDataFactory.build();
        const mockDataToResponse = mockDataFactory.build();

        const mockedFetcher = mock<Fetcher>();
        mockedFetcher.post
            .calledWith(urlToCall, mockData)
            .mockResolvedValue(mockDataToResponse);

        const mockDataResponse = await new HttpClient(mockedFetcher).post<MockData>(
            urlToCall,
            mockData,
        );

        expect(mockDataToResponse.firstName).toEqual(mockDataResponse?.firstName);
        expect(mockDataToResponse.lastName).toEqual(mockDataResponse?.lastName);
        expect(mockedFetcher.post).toBeCalledWith(urlToCall, mockData);
    });

    it("Should be called put with success", async () => {
        const urlToCall = "anyUrl";

        const mockData = mockDataFactory.build();
        const mockDataToResponse = mockDataFactory.build();

        const mockedFetcher = mock<Fetcher>();
        mockedFetcher.put
            .calledWith(urlToCall, mockData)
            .mockResolvedValue(mockDataToResponse);

        const mockDataResponse = await new HttpClient(mockedFetcher).put<MockData>(
            urlToCall,
            mockData,
        );

        expect(mockDataToResponse.firstName).toEqual(mockDataResponse?.firstName);
        expect(mockDataToResponse.lastName).toEqual(mockDataResponse?.lastName);
        expect(mockedFetcher.put).toBeCalledWith(urlToCall, mockData);
    });

    it("Should be called delete with success", async () => {
        const urlToCall = "anyUrl";

        const mockDataToResponse = mockDataFactory.build();

        const mockedFetcher = mock<Fetcher>();
        mockedFetcher.delete.calledWith(urlToCall).mockResolvedValue(mockDataToResponse);

        const mockDataResponse = await new HttpClient(mockedFetcher).delete<MockData>(
            urlToCall,
        );

        expect(mockDataToResponse.firstName).toEqual(mockDataResponse?.firstName);
        expect(mockDataToResponse.lastName).toEqual(mockDataResponse?.lastName);
        expect(mockedFetcher.delete).toBeCalledWith(urlToCall);
    });
});
