import nock, { cleanAll } from "nock";

import { HttpClientFactory } from "../../../../src/HttpClient/src/Client/HttpClientFactory";
import { HttpException } from "../../../../src/HttpClient/src/Client/HttpException";
import { FetcherConfig } from "../../../../src/HttpClient/src/Fetcher/FetcherConfig";
import { mockDataFactory } from "../dataBuilders/MockDataFactory";
import { getError } from "../utils/utils";

describe("Component test", () => {
    beforeEach(() => {
        jest.clearAllMocks();
        cleanAll();
    });

    it("Should be returned success get axios", async () => {
        const mockData = mockDataFactory.buildList(3);

        const fetcherConfig: FetcherConfig = {
            baseUrl: "http://localhost",
            retryCount: 1,
            timeout: 30000,
            retryDelay: 300,
        };

        nock(fetcherConfig.baseUrl).get("/test").reply(200, mockData);

        const factory = new HttpClientFactory(fetcherConfig);

        const response = await factory.httpClient.get("test");

        expect(response).toEqual(mockData);
    });

    it("Should be returned error get axios", async () => {
        const fetcherConfig: FetcherConfig = {
            baseUrl: "http://localhost",
            retryCount: 0,
            timeout: 30000,
            retryDelay: 300,
        };
        const codeError = "error.get.code";
        const messageError = "error.get.message";
        const httpCodeError = 422;

        nock(fetcherConfig.baseUrl).get("/test").reply(httpCodeError, {
            code: codeError,
            message: messageError,
        });

        const factory = new HttpClientFactory(fetcherConfig);

        const error = await getError<HttpException>(
            async () => await factory.httpClient.get("test"),
        );

        expect(error).toBeInstanceOf(HttpException);
        expect(error).toHaveProperty("httpCode", httpCodeError);
    });
});
