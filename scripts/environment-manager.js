const download = require("download");
const fs = require("fs");

const ENV_MAPPER = {
    dev: {
        fileName: ".env.development",
        id: "2411272",
    },
};

const PATH_TO_SAVE_ENV_FILE = "./env";

async function createPath() {
    await fs.promises.mkdir(PATH_TO_SAVE_ENV_FILE, { recursive: true }, (error) => {
        if (!error) {
            console.log("New directory successfully created.");
        } else{
            throw new Error("It wasn't possible create env folder.")
        }
    });
}

async function getDevelopmentEnv(environment) {
    try {
        await createPath();

        const envMapper = ENV_MAPPER[environment];

        const pathToSaveFile = `${PATH_TO_SAVE_ENV_FILE}/${envMapper.fileName}`;       

        const envAlreadyExists = fs.existsSync(pathToSaveFile);

        if (!envAlreadyExists) {
            const snippetUrl = `https://gitlab.com/api/v4/snippets/${envMapper.id}/files/main/${envMapper.fileName}/raw`;

            download(snippetUrl).pipe(
                fs.createWriteStream(pathToSaveFile)
            );
        }

        console.log(`The env file ${envMapper.fileName} was generated with success on ./env folder`);

    } catch (error) {
        console.log(`It wasn't possible generate env file. ${error}`);
        process.exit(1);
    }
}

getDevelopmentEnv(process.argv[2]);
